/*NORMALISIERTE DATENBANK BEFÜLLEN*/

/*T_person füllen*/
INSERT INTO t_person (vorname, geburtsname, nachname, geburtstag, todestag, todesort)
    SELECT DISTINCT vorname vorname, geburtsname geburtsname, nachname nachname, geburtsname geburtstag, todestag todestag, todesort todesort FROM stolperstein;


/*T_person füllen*/
INSERT INTO t_adresse (adresse, ortsteil)
  SELECT DISTINCT adresse adresse, ortsteil ortsteil FROM stolperstein;


/*T_deportation*/
INSERT INTO t_deportation (deportationsziel)
  SELECT DISTINCT erstes_deportationsziel deportationsziel FROM stolperstein
  UNION SELECT DISTINCT zweites_deportationsziel deportationsziel FROM stolperstein
  UNION SELECT DISTINCT drittes_deportationsziel deportationsziel FROM stolperstein;

