/*Normalisierung der Datenbank*/
CREATE TABLE stolperstein_db.t_person (
  P_P_ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  vorname VARCHAR(25),
  geburtsname VARCHAR(25),
  nachname VARCHAR(25),
  geburtstag VARCHAR(36),
  todestag VARCHAR(36),
  todesort VARCHAR(36),
  F_A_ID INT
);

CREATE TABLE stolperstein_db.t_deportation (
  P_D_ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  deportationsziel VARCHAR(36)
);

CREATE TABLE stolperstein_db.t_adresse (
  P_A_ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  adresse VARCHAR(125),
  ortsteil VARCHAR(25)
);

CREATE TABLE stolperstein_db.t_person_deportation (
  F_P_ID INT NOT NULL,
  F_D_ID INT NOT NULL,
  deportationstag VARCHAR(36),
  deportationsnummer INT
);