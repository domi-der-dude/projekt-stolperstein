/*Tabellen erstellen*/
CREATE TABLE stolperstein_db.stolperstein(
  ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  vorname VARCHAR(25),
  geburtsname VARCHAR(25),
  nachname VARCHAR(25),
  geburtstag VARCHAR(36),
  adresse VARCHAR(125),
  ortsteil VARCHAR(25),
  erster_deportationstag VARCHAR(36),
  erstes_deportationsziel VARCHAR(36),
  zweiter_deportationstag VARCHAR(36),
  zweites_deportationsziel VARCHAR(36),
  dritter_deportationstag VARCHAR(36),
  drittes_deportationsziel VARCHAR(36),
  todestag VARCHAR(36),
  todesort VARCHAR(36)
);

LOAD DATA LOCAL INFILE '/Applications/MAMP/htdocs/stolperstein/files/stolpersteine.csv'
INTO TABLE stolperstein
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
(vorname, geburtsname,nachname,geburtstag,adresse,ortsteil,erster_deportationstag,erstes_deportationsziel, zweiter_deportationstag,zweites_deportationsziel,dritter_deportationstag,drittes_deportationsziel,todestag,todesort);
