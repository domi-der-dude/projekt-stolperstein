
<footer>
    Made by Natascha & Dominik
</footer>

<script>
    //Ort in Variabel festlegen
    var mymap = L.map('mapid').setView([52.517547, 13.453995], 12);


    //Karte an bestimmtem Ort anzeigen
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        id: 'mapbox.streets'
    }).addTo(mymap);

</script>


</body>
</html>