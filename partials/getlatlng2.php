<?php

// Daten aus der Datenbank holen und in Arrays abspeichern

$mysqli = new mysqli("localhost", "root", "root", "stolperstein_db");
if ($mysqli->connect_errno) {
    die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
}

$sql = "SELECT * FROM stolperstein";
$statement = $mysqli->prepare($sql);
$statement->execute();

$result = $statement->get_result();

$resultAddress = [];
$resultLastName = [];
$resultFirstName = [];
$gebDat = [];
$totDat = [];
$totOrt = [];
$ort = [];
$ersteDepTag = [];
$zweiteDepTag = [];
$dritteDepTag = [];
$ersteDepZiel = [];
$zweiteDepZiel = [];
$dritteDepZiel = [];
$coordLat = [];
$coordLong = [];


while($row = $result->fetch_object()) {
    array_push($resultAddress, $row->adresse);
    array_push($resultLastName, $row->nachname);
    array_push($resultFirstName, $row->vorname);
    array_push($gebDat, $row->geburtstag);
    array_push($totDat, $row->todestag);
    array_push($totOrt, $row->todesort);
    array_push($ort, $row->ortsteil);
    array_push($ersteDepTag, $row->erster_deportationstag);
    array_push($zweiteDepTag, $row->zweiter_deportationstag);
    array_push($dritteDepTag, $row->dritter_deportationstag);
    array_push($ersteDepZiel, $row->erstes_deportationsziel);
    array_push($zweiteDepZiel, $row->zweites_deportationsziel);
    array_push($dritteDepZiel, $row->drittes_deportationsziel);
    array_push($coordLat, $row->lat);
    array_push($coordLong, $row->lng);
}

// Ausgeben aller Datensätze
/*for($i = 0; $i < sizeof($coordLong); $i++){
    echo $coordLong[$i];
}*/

// Leerzeichen in Adresse durch Bindestriche ersetzen
for($i = 0; $i < sizeof($resultAddress); $i++) {
    $resultAddress[$i] = str_replace(" ", "-", $resultAddress[$i]);
}

// Auskommentieren um automatisches Update zu verhindern
// Adressen durchlaufen an Google senden, Lat Long in Variabel speichern und in Datenbank einpflegen
// Aktueller Stand 5500!

/*for($i = 5500; $i < 6000; $i++){
    $request = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=" . $resultAddress[$i] . "&key=AIzaSyD4Wa1dWvs03MToiqq-AS_vVShDFDVLMy0");
    $json = json_decode($request);
    $xlong = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
    $xlat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};

    echo '<br><br>';

// Create connection
    $conn = new mysqli(localhost, root, root, stolperstein_db);
// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $setLat = "UPDATE stolperstein SET lat = " . $xlat . " WHERE id = " . ($i+1);
    $setLng = "UPDATE stolperstein SET lng = " . $xlong . " WHERE id = " . ($i+1);

    if ($conn->query($setLat) === TRUE) {
        echo "New lat created successfully" . '<br>';
    } else {
        echo "Error: " . $setLat . "<br>" . $conn->error;
    }

    if ($conn->query($setLng) === TRUE) {
        echo "New long created successfully" . '<br>';
    } else {
        echo "Error: " . $setLng . "<br>" . $conn->error;
    }

    mysqli_close($conn);
}*/

$mysqli->close();

?>
