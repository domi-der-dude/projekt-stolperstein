<script>
//Marker hinzufügen
var street = <?php echo json_encode($resultAddress); ?>;
var lastName = <?php echo json_encode($resultLastName); ?>;
var firstName = <?php echo json_encode($resultFirstName); ?>;
var gebDat = <?php echo json_encode($gebDat); ?>;
var totDat = <?php echo json_encode($totDat); ?>;
var totOrt = <?php echo json_encode($totOrt); ?>;
var ort = <?php echo json_encode($ort); ?>;
var ersteDepTag = <?php echo json_encode($ersteDepTag); ?>;
var zweiteDepTag = <?php echo json_encode($zweiteDepTag); ?>;
var dritteDepTag = <?php echo json_encode($dritteDepTag); ?>;
var ersteDepZiel = <?php echo json_encode($ersteDepZiel); ?>;
var zweiteDepZiel = <?php echo json_encode($zweiteDepZiel); ?>;
var dritteDepZiel = <?php echo json_encode($dritteDepZiel); ?>;
var lat = <?php echo json_encode($coordLat); ?>;
var long = <?php echo json_encode($coordLong); ?>;


var allMarkers = L.layerGroup([]);
var marker = [];

//Set All Stumbling Stones
(function() {

    //foreach -> Datensätze durchlaufen und Marker füllen
    for(var i = 0; i < 1000; i++) {

        marker[i] = L.circleMarker([lat[i], long[i]]).bindPopup("<b>" + lastName[i] + "," + firstName[i] + "</b><br>*" + gebDat[i] + " " + totDat[i]);

        marker[i].addTo(allMarkers);
    }

    allMarkers.addTo(mymap);

})();

function setFilter() {
    mymap.removeLayer(allMarkers);

    var region = document.getElementById('region').value;

/*
* sql abfrage mit suchkriterien
* */

    var filteredMarker = L.LayerGroup([]);

    <?php
    $ortAbfrage = 'SELECT * FROM stolperstein WHERE ortsteil = "' . region .'";'
    ?>

    for(var i = 0; i < 1000; i++) {

        marker[i] = L.circleMarker([lat[i], long[i]]).bindPopup("<b>" + lastName[i] + "," + firstName[i] + "</b><br>*" + gebDat[i] + " " + totDat[i]);

        marker[i].addTo(filteredMarker);
    }

    filteredMarker.addTo(mymap);

    /*
    var suchbegriff = document.getElementById('searchKey').value;
    console.log(region, suchbegriff);
    */
}

</script>
