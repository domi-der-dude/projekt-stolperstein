<?php

//Header einfügen
include 'partials/header.php';

?>

<?php

//CSV Datei in Variabel speichern
/*$csv_datei = "files/stolpersteine.csv";

$felder_trenner = ",";
$zeilen_trenner = "\n";*/


/*//Prüfen ob die Datei existiert ansonsten Fehlermeldung
if (@file_exists($csv_datei) == false) {

    echo 'Die CSV Datei: '. $csv_datei.' gibt es nicht!' . "<br>";

} else {

    //Inhalt der Datei in Variabel speichern
    $datei_inhalt = @file_get_contents($csv_datei);


    //Array mit Inhalt nach Zeilen getrennt
    $zeilen = explode($zeilen_trenner,
        $datei_inhalt);

    //Ermittelt die Anzahl der Zeilen der CSV
    $anzahl_zeilen = count($zeilen);

}*/


?>

<?php include 'partials/getAllRegions.php.php'; ?>
<?php include 'partials/getlatlng2.php'; ?>

<div class="intro">
    <img class="intro--pic" src="img/st_grafik.png" alt="stolperstein-blank">
    <div class="intro--text">
        <div class="intro--text__valign">
            <h1>Projekt</h1>
            <h2>Stolperstein</h2>
            <p>
               <!-- --><?php /*echo 'Eine Übersicht über ' . $anzahlStolpersteine .' Stolpersteine in Berlin.<br>'; */?>
            </p>
            <button id="scrollMap">explore</button>
        </div>
    </div>
</div>

<div class="stumbling-filter">
    <div class="stumbling-filter__wrapper">
        <div class="stumbling-filter__region">
            Wähle einen Bezirk:
            <select name="region" id="region">
                <option value="mitte">Mitte </option>
                <option value="tiergarten">Tiergarten </option>
                <option value="wedding">Wedding </option>
                <option value="pberg">Prenzlauer Berg </option>
                <option value="fhain">Friedrichshain </option>
                <option value="xberg">Kreuzberg </option>
                <option value="chb">Charlottenburg </option>
                <option value="spandau">Spandau </option>
                <option value="wilmersdorf">Wilmersdorf </option>
                <option value="zehlendorf">Zehlendorf </option>
                <option value="schoeneberg">Schöneberg </option>
                <option value="steglitz">Steglitz </option>
                <option value="tempelhof">Tempelhof </option>
                <option value="neukoelln">Neukölln </option>
                <option value="treptow">Treptow</option>
                <option value="koepenick">Köpenick</option>
                <option value="lichtenberg">Lichtenberg</option>
                <option value="weissensee">Weißensee</option>
                <option value="pankow">Pankow</option>
                <option value="reinickendorf">Reinickendorf</option>

            </select></div>
        <div class="stumbling-filter__search"><input id="searchKey" placeholder="Suchbegriff" type="text"></div>
        <div class="stumbling-filter__search"><button onclick="setFilter()">GO</button></div>
    </div>
</div>

<div class="stumbling-map">
    <div id="mapid" name="mapip" class="mapid"></div>
</div>

<?php include 'partials/addMarker.php'; ?>

<?php include 'partials/footer.php'; ?>
<?php include 'partials/setMarker.php'; ?>
